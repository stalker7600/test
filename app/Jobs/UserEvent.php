<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 18.07.2018
 * Time: 17:18
 */

namespace App\Jobs;

use App\CQRS\Job;
use App\EventsLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserEvent extends Job implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $eventId;

    /**
     * UserEvent constructor.
     * @param $id
     * @param $eventId
     */
    public function __construct($id, $eventId)
    {
        $this->id      = $id;
        $this->eventId = $eventId;
    }


    public function handle()
    {
        $eventLog = EventsLog::query()->where('user_id', $this->id)->where('event_id', $this->eventId)->first();

        $prevLog = EventsLog::query()
            ->where('id', '<', $eventLog->id)
            ->where('user_id', $this->id)
            ->orderByDesc('id')
            ->first();

        // Эмулируем ошибку
        $error = rand(1, 10) === 5;

        if (!$error && ($prevLog === null || $prevLog->processed === true)) {
            $eventLog->processed = true;
            $eventLog->save();

            file_put_contents(
                storage_path('app/test/log_for_user' . $this->id),
                'user_id => ' . $this->id . ' event_id => ' . $this->eventId . PHP_EOL,
                FILE_APPEND
            );
            sleep(1);

            return;
        }
        //типо ошибка
        error_log('ERRRRROR for user_id => ' . $this->id . ' event_id => ' . $this->eventId);
        throw new \Exception('error');
    }

}
