<?php

namespace App\Console\Commands;

use App\EventsLog;
use App\Jobs\UserEvent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateEvents extends Command
{

    /**
     * The name and signature of the console command.
     * php artisan events:generate
     *
     * @var string
     */
    protected $signature = 'events:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'events:generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // чистим таблицу
        EventsLog::query()->truncate();

        // чистим логи
        if (file_exists(storage_path('app/test'))) {
            foreach (glob(storage_path('app/test') . '/*') as $file) {
                unlink($file);
            }
        }

        $this->generateUsers();
        for ($i = 0; $i < 100000; $i++) {
            $user = $this->getUser();
            if (!$user) {
                error_log('END!');

                return null;
            }
            $this->info('event ' . $i . 'generated for user' . $user);
            DB::transaction(
                function () use ($user, $i) {
                    EventsLog::create(
                        [
                            'user_id'   => $user,
                            'event_id'  => $i,
                            'processed' => false,
                        ]
                    );
                    dispatch((new UserEvent($user, $i))->onQueue('parse'));
                }
            );
        }
        $this->info('generated');
    }

    private $users = [];

    private function generateUsers()
    {
        for ($i = 1; $i <= 1000; $i++) {
            $this->users[$i] = 0;
        }
    }

    private function getUser()
    {
        if (!count($this->users)) {
            return null;
        }
        $userId               = array_rand($this->users);
        $this->users[$userId] += 1;
        if ($this->users[$userId] === 10) {
            unset($this->users[$userId]);
        }

        return $userId;
    }
}
